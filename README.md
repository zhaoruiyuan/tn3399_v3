# TN3399_V3

> 代码仓地址  : https://gitee.com/zhaoruiyuan/tn3399_v3_project

> 源码克隆下载: git clone https://gitee.com/zhaoruiyuan/tn3399_v3_project.git

## 规格参数

|   部件名称    |       芯片型号       |                           备注说明                           |
| :-----------: | :------------------: | :----------------------------------------------------------: |
|      CPU      |        RK3399        | Dual-core Cortex-A72 up to 1.8GHz;Quad-core Cortex-A53 up to 1.4GHz;Mali-T864 GPU |
|      RAM      |       K4B8G16        |                  Dual-channel DDR3 1GB * 4                   |
|     Flash     | SanDisk SDINBDG4-16G |                           eMMC 5.1                           |
|      PMU      |        RK808D        |                                                              |
|   Ethernet    |       RTL8211E       |                      10/100/1000 Base-T                      |
|    WIFI+BT    |        AP6255        |               WIFI IEEE802.11 a/b/g/n/ac;BT4.2               |
|   SATA 3.0    |        JMS578        |                                                              |
|    USB 2.0    |        FE1.1s        |     TYPE A Mount Socket * 2 & 4-Pin Connector Socket * 5     |
|    USB 3.0    |       VL817-Q7       |                   TYPE A Mount Socket * 2                    |
|     UART      |      SP3232EEN       |                                                              |
| HDMI 2.0+LVDS |  358775G + ALC5640   |                                                              |
|   Audio PA    |        NS4258        |                            5W * 2                            |

## 启动流程

![启动流程](assets/rockchip_bootflow.png)

## 编译环境搭建

### Linux开发系统搭建
```shell
安装虚拟机 参看doc搭建文档


# 更换软件源
cp -a /etc/apt/sources.list /etc/apt/sources.list.bak
sed -i "s@http://.*archive.ubuntu.com@http://mirrors.huaweicloud.com@g" /etc/apt/sources.list
sed -i "s@http://.*security.ubuntu.com@http://mirrors.huaweicloud.com@g" /etc/apt/sources.list

# 更新软件包
sudo apt-get install update && apt upgrade -y

# 安装最新的软件库
sudo apt-get install build-essential gcc g++ make zlib* libffi-dev e2fsprogs pkg-config flex bison perl bc openssl libssl-dev libelf-dev libc6-dev-amd64 binutils binutils-dev libdwarf-dev u-boot-tools mtd-utils gcc-arm-linux-gnueabi cpio device-tree-compiler
sudo apt-get install build-essential gcc g++ make zlib* libffi-dev
sudo apt-get install dosfstools mtools mtd-utils default-jre default-jdk
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev 
sudo apt-get install gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache sudo apt-get install libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby
sudo apt install nodejs -y
sudo apt install npm -y
sudo apt-get install libncurses5
sudo apt-get install libtinfo5
sudo apt-get install genext2fs
sudo apt-get install liblz4-tool
sudo apt-get install gawk

# 创建工作目录

cd ~
mkdir tn3399_v3
cd tn3399_v3

使用克隆命令 将代码克隆到本地 

#对应开发文档库
git clone https://gitee.com/zhaoruiyuan/tn3399_v3.git
#对应代码维护库
git clone https://gitee.com/zhaoruiyuan/tn3399_v3_project.git


#通过tree -L 2 来查看项目目录结构
tn3399_v3
├── tn3399_v3_doc
│   ├── assets
│   └── README.md
└── tn3399_v3_project
    ├── kernel
    ├── README.md
    ├── tools
    └── uboot

```

### 工具链

```shell

# 工具链对应位置
~/tn3399_v3/tn3399_v3_project/tools

# 配置环境变量 根据自己代码响相应位置配置
export PATH=~/tn3399_v3/tn3399_v3_project/tools/gcc-arm-none-eabi-9-2020-q2-update/bin:$PATH
export PATH=~/tn3399_v3/tn3399_v3_project/tools/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin:$PATH
# 测试
aarch64-linux-gnu-gcc -v
arm-none-eabi-gcc -v

# 安装辅助工具
apt install -y build-essential libncurses5-dev git make
```

### 编译tn3399的u-boot

```shell
# 不使用项目指定工具链
sed -i 's@TOOLCHAIN_ARM32=.*@TOOLCHAIN_ARM32=/@g' ./make.sh
sed -i 's@TOOLCHAIN_ARM64=.*@TOOLCHAIN_ARM64=/@g' ./make.sh
sed -i 's@${absolute_path}/bin/@@g' ./make.sh

# 编译修改过的u-boot 参数evb-rk3399配置选项对应config文件夹里面的 evb-rk3399_deconfig文件
./make.sh evb-rk3399

```
得到uboot.img和rk3399_loader_v1.24.126.bin，等下需要

### 制作系统刷机包组件镜像
#### 编译tn3399_v3的刷机工具
```shell

1.需要先编译Linux 版本的tn3399_v3的刷机工具 可以通过命令pwd 查看
    进入源码目录下面 /tn3399_v3/tn3399_v3_project/uboot/rkdeveloptool
        1.1 安装 libusb and libudev 库
	        sudo apt-get install libudev-dev libusb-1.0-0-dev dh-autoreconf
        2.1 设置root模式 ubuntu一般需要设置管理员用户
            2.1.1 sudo posswd
                  当前用户的密码：
                  root新密码
                  root确认密码
                  su root 进入系统管理员模式
        3.1 aclocal
        4.1 autoreconf -i
        5.1 autoheader
        6.1 automake --add-missing
        7.1 ./configure
        8.1  make 
                 

```
#### 制作idbloader.img

当前路径通过pwd命令查看
    /tn3399_v3/tn3399_v3_project/uboot/u-boot-next-dev

```shell
# 找到rkbin带的ddr初始化文件，用u-boot里面的mkimage制作idbloader.img
../rkbin/tools/mkimage -n rk3399 -T rksd -d ../rkbin/bin/rk33/rk3399_ddr_800MHz_v1.25.bin idbloader.img

# 再往后写miniloader
cat ../rkbin/bin/rk33/rk3399_miniloader_v1.26.bin >> idbloader.img

rk3399_ddr_800MHz_v1.25.bin 二进制bin文件主要是初始化ddr内存相关机制
rk3399_miniloader_v1.26.bin 加载uboot镜像有关代码

```
得到idbloader.img文件 刷机需要以下镜像文件
    1.idbloader.img
    2.uboot.img
    3.trus.img

#### 了解一下怎么刷emmc

rk家的rom code支持usb启动，不怕弄成砖头

首先上电，插上microusb，安装rkdeveloptool

然后短接这两个脚，保持短接，按reset

![刷机短接点](assets/rockchip_reset.png)

这步是把emmc某个数据脚接地，让rom code找不到emmc,进入usb启动

然后用rkdeveloptool初始化ddr

```shell

#通过 su root 切换为超级管理员模式

root@toutes-RH2288-V3:~/tn3399_v3/tn3399_v3_project/uboot/u-boot-next-dev$ ../rkdeveloptool/rkdeveloptool ld
not found any devices!
# 短接复位后
root@toutes-RH2288-V3:~/tn3399_v3/tn3399_v3_project/uboot/u-boot-next-dev$ ../rkdeveloptool/rkdeveloptool ld
DevNo=1	Vid=0x2207,Pid=0x330c,LocationID=302	Maskrom

#加载系统load镜像
root@toutes-RH2288-V3:~/tn3399_v3/tn3399_v3_project/uboot/u-boot-next-dev$ 
sudo ../rkdeveloptool/rkdeveloptool db rk3399_loader_v1.25.126.bin
Downloading bootloader succeeded.

sudo ../rkdeveloptool/rkdeveloptool wl 0x40   idbloader.img
sudo ../rkdeveloptool/rkdeveloptool wl 0x2000 uboot.img
sudo ../rkdeveloptool/rkdeveloptool wl 0x4000 trust.img
sudo ../rkdeveloptool/rkdeveloptool wl 0x6000 resource.img
sudo ../rkdeveloptool/rkdeveloptool gpt parameter.txt
sudo ../rkdeveloptool/rkdeveloptool rd


```

#烧写uboot引导进行
![刷机load](assets/rockchip_load.png)

#代码烧写完以后退出超级管理员模式 su toutes

tips: uboot两个配置宏
```shell
#配置uboot bootdelay 时间宏
CONFIG_BOOTDELAY=10

#配置uboot saveenv 写配置保存命令宏
CONFIG_CMD_SAVEENV=y
CONFIG_ENV_IS_IN_MMC=y

```

刷完之后reset，不出意外串口就可以正常输出，进u-boot了

