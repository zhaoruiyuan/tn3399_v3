# TN3399_V3 适配HDMI图像显示功能
> 代码仓地址  : https://gitee.com/zhaoruiyuan/tn3399_v3_project

## 显示图片展示

![显示图片展示](../assets/rockchip_poweron.png)

## uboot开机显示logo代码流程分析

函数入口部分
uboot\u-boot-next-dev\common\board_r.c
```code
#ifdef CONFIG_BOARD_LATE_INIT
	board_late_init,
#endif
```

函数声明部分
u-boot\arch\arm\mach-rockchip\board.c
```code
int board_late_init(void)
{
	rockchip_show_logo();
	return rk_board_late_init();
}
```

rockchip_show_logo();函数实现体
u-boot-next-dev\drivers\video\drm\rockchip_display.c
```code
int rockchip_show_logo(void)
{
	struct display_state *s;
	int ret = 0;
	printf("rockchip_show_logo start\n"); //新增函数打印信息 start
	list_for_each_entry(s, &rockchip_display_list, head) {
		s->logo.mode = s->logo_mode;
		if (load_bmp_logo(&s->logo, s->ulogo_name))
			printf("failed to display uboot logo\n");
		else
			ret = display_logo(s);

		/* Load kernel bmp in rockchip_display_fixup() later */
	}
	printf("rockchip_show_logo end\n"); //新增函数打印信息 start
	return ret;
}

```

编译烧写uboot镜像,通过串口终端打印uboot信息 可以看到如下信息
```code
rockchip_show_logo start
No resource partition
No file: logo.bmp
failed to display uboot logo
rockchip_show_logo end
```


分析rockchip_display_probe(struct udevice *dev)
```code

```



代码流程分析
```code
1.board_late_init
	charge_display();//充电相关动画显示
	rockchip_show_logo();//uboot开机动画显示
		load_bmp_logo(&s->logo, s->ulogo_name);//获取load bpm信息
			rockchip_read_resource_file //获取resource文件
				get_file_info //获取文件info信息
					init_resource_list //初始化resource列表
						get_resource_base_sector //resource 扇区信息
							part_get_info_by_name //查找 resource 扇区信息
```



tips: 关于resource.img镜像小知识 =>

```code
需要提前编译u-boot源代码
~/tn3399_v3/tn3399_v3_project/uboot/u-boot-next-dev/tools/resource_tool  //需要手动执行制作resource.img镜像文件

//resource.img 镜像解包操作
sudo ./resource_tool --verbose --unpack --image=resource.img //输出dump信息

Dump header:
partition version:0.0
header size:1
index tbl:
        offset:1        entry size:1    entry num:3
Dump Index table:
entry(0):
        path:rk-kernel.dtb
        offset:4        size:106264
D/dump_file(1263): try to dump entry:rk-kernel.dtb
D/mkdirs(1250): mkdir:out
entry(1):
        path:logo.bmp
        offset:212      size:2764856
D/dump_file(1263): try to dump entry:logo.bmp
D/mkdirs(1250): mkdir:out
entry(2):
        path:logo_kernel.bmp
        offset:5613     size:2764856
D/dump_file(1263): try to dump entry:logo_kernel.bmp
D/mkdirs(1250): mkdir:out
Unack resource.img to out successed!

toutes@toutes-RH2288-V3:~/temp/out$ tree
.
├── logo.bmp
├── logo_kernel.bmp
└── rk-kernel.dtb

//resource.img 镜像打包操作 
sudo ./resource_tool --verbose --pack out/rk-kernel.dtb logo.bmp logo_kernel.bmp resource.img

.
├── logo.bmp
├── logo_kernel.bmp
├── out
│   └── rk-kernel.dtb
├── resource.img
└── resource_tool

```

//设备树 解压dtb 测试
```code

dtc -I dtb -O dts -o rk-kernel.dtb rk-kernel.dts
生成dts文件,对应生成设备树文件

```




part_get_info_by_name  函数分析
```code
#define PART_RESOURCE			"resource"
#define RESOURCE_MAGIC			"RSCE"
#define RESOURCE_MAGIC_SIZE		4
#define RESOURCE_VERSION		0
#define CONTENT_VERSION			0
#define ENTRY_TAG			"ENTR"
#define ENTRY_TAG_SIZE			4
#define MAX_FILE_NAME_LEN		220
#define MAX_HASH_LEN			32
#define DTB_FILE			"rk-kernel.dtb"



/* Part types */
#define PART_TYPE_UNKNOWN	0x00
#define PART_TYPE_MAC		0x01
#define PART_TYPE_DOS		0x02
#define PART_TYPE_ISO		0x03
#define PART_TYPE_AMIGA		0x04
#define PART_TYPE_EFI		0x05
#define PART_TYPE_RKPARM	0x06




int part_get_info_by_name(struct blk_desc *dev_desc, const char *name,
			  disk_partition_t *info)
{
	part_drv = part_driver_lookup_type(dev_desc); //获取引导信息,匹配相同的引导模式 并且返回处理
	if (!part_drv) {
		debug("part_get_info_by_name ret:=%d\n", -1);
		return -1;

retry:
	//printf("## Query partition(%d): %s\n", none_slot_try, name_slot);
	debug("part_get_info_by_name:max_entries: =%d\n", part_drv->max_entries);
	for (i = 1; i < part_drv->max_entries; i++) { 	//获取组件个数信息 找到并且与之匹配并且返回
		ret = part_drv->get_info(dev_desc, i, info);
		debug("part_get_info_by_name ret = %d, info->name: %s\n", ret, (const char *)info->name);
		if (ret != 0) {
			/* no more entries in table */
			break;
		}
		if (strcmp(name_slot, (const char *)info->name) == 0) {  //找到并且与之匹配并且返回组件信息
			/* matched */
			return i;
		}
	}

	/* 2. Query partition without A/B slot suffix if above failed */
	if (none_slot_try) {
		none_slot_try = 0;
		strcpy(name_slot, name);
		goto retry;
	}

	return -1;
}




```

由于 part_get_info_by_name 调用的位置过多打印的信息比较乱不好只管分析,因此将思想抽象出来单独调用分析
tips:
```code

static int get_board_parameterlist(void)
{
	printf("get_board_parameterlist start....\n");
	struct part_driver *entry;
	struct blk_desc *dev_desc;
	disk_partition_t info;
	int i = 0;
	int ret=0;

	struct part_driver *drv = ll_entry_start(struct part_driver, part_driver);
	const int n_ents = ll_entry_count(struct part_driver, part_driver);


	dev_desc = rockchip_get_bootdev();
	for (entry = drv; entry != drv + n_ents; entry++) {
		printf("get_board_parameterlist: \n");
		printf("name:%s\n", entry->name);
		printf("part_type:%d\n", entry->part_type);
		printf("max_entries:%d\n", entry->max_entries);
		if (dev_desc->part_type == entry->part_type) {
			for (i = 1; i < entry->max_entries; i++) {
				ret = entry->get_info(dev_desc, i, &info);
				printf("part_get_info_by_name ret = %d, info->name: %s\n", ret, (const char *)(info.name));
				if (ret != 0) {
					/* no more entries in table */
					break;
				}
			}
		}
	}

	return 0;
}

```

输出终端信息如下：

```code
part_get_info_by_name ret = 0, info->name: uboot
part_get_info_by_name ret = 0, info->name: trust
part_get_info_by_name ret = 0, info->name: misc
part_get_info_by_name ret = 0, info->name: boot
part_get_info_by_name ret = 0, info->name: recovery
part_get_info_by_name ret = 0, info->name: backup
part_get_info_by_name ret = 0, info->name: rootfs
part_get_info_by_name ret = 0, info->name: userdata

```

通过 part list mmc 0 命令查看
```code
=> part list mmc 0

part list mmc 0 命令分析 => 

Partition Map for MMC device 0  --   Partition Type: EFI

Part    Start LBA       End LBA         Name
        Attributes
        Type GUID
        Partition GUID
  1     0x00004000      0x00005fff      "uboot"
        attrs:  0x0000000000000000
        type:   e4380000-0000-4450-8000-5b5900006ee9
        guid:   9e620000-0000-4212-8000-2b6c00002099
  2     0x00006000      0x00007fff      "trust"
        attrs:  0x0000000000000000
        type:   7e020000-0000-4d41-8000-0f1700007dc1
        guid:   79070000-0000-4b16-8000-3d6f00003a05
  3     0x00008000      0x00009fff      "misc"
        attrs:  0x0000000000000000
        type:   c4740000-0000-4f04-8000-0aca00004d4b
        guid:   d4720000-0000-404a-8000-78bc00007db8
  4     0x0000a000      0x00029fff      "boot"
        attrs:  0x0000000000000000
        type:   5e5f0000-0000-427b-8000-451a000073b1
        guid:   565e0000-0000-4a18-8000-7dfa000049da
  5     0x0002a000      0x00049fff      "recovery"
        attrs:  0x0000000000000000
        type:   f3690000-0000-4d18-8000-09b100001918
        guid:   a85c0000-0000-417f-8000-40d200001092
  6     0x0004a000      0x00059fff      "backup"
        attrs:  0x0000000000000000
        type:   2b0c0000-0000-4966-8000-763400007baa
        guid:   72500000-0000-4c11-8000-0bec00005a08
  7     0x0005a000      0x006d83ff      "rootfs"
        attrs:  0x0000000000000000
        type:   b0150000-0000-4e7f-8000-7e4e00006c8f
        guid:   614e0000-0000-4b53-8000-1d28000054a9
  8     0x006d8400      0x01d59fbf      "userdata"
        attrs:  0x0000000000000000
        type:   27270000-0000-4313-8000-68a500004fef
        guid:   0b490000-0000-4064-8000-7e84000078d1

根据终端信息输出可以看出 当前引导属于PART_TYPE EFI引导

```

配置 parameter.txt配置分区信息表 详细参看parameter.txt

使用gpt 命令烧写parameter_gpt.txt到gpt分区
sudo ../rkdeveloptool/rkdeveloptool gpt parameter.txt

```code

get_board_parameterlist start....
get_board_parameterlist:
name:EFI
part_type:5
max_entries:128
part_get_info_by_name ret = 0, info->name: uboot
part_get_info_by_name ret = 0, info->name: trust
part_get_info_by_name ret = 0, info->name: resource


=> part list mmc 0

Partition Map for MMC device 0  --   Partition Type: EFI

Part    Start LBA       End LBA         Name
        Attributes
        Type GUID
        Partition GUID
  1     0x00002000      0x00003fff      "uboot"
        attrs:  0x0000000000000000
        type:   8dcd1602-5214-43f9-ffef-c49a4ee450b7
        guid:   5a30833f-8c43-47b1-a41d-84d40883d0c6
  2     0x00004000      0x00005fff      "trust"
        attrs:  0x0000000000000000
        type:   1124e418-9008-41f8-9e3d-8d872216c8a1
        guid:   67ad7741-4638-47c6-a488-d396583e582a
  3     0x00006000      0x00007fff      "resource"
        attrs:  0x0000000000000000
        type:   fbd4486a-e42c-448d-d4e9-7f5c324a7843
        guid:   3adea03d-c612-46ed-cff9-b79439f11561
=>


```


